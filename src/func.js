const getSum = (str1, str2) => {
  let a = str1,
      b = str2;
  if(typeof str1 === 'string' && typeof str2 === 'string'){
    if(str1.length === 0){ 
      a = '0';
    }
    if(str2.length === 0){
      b = '0';
    }
    if(/\d/.test(a) && /\d/.test(b)){
      return (+a + +b + '') ;
    }
  }else{
    return false;
  }
};
console.log(getSum('123', '123'))

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0,
      comments = 0;
  for(let i of listOfPosts){
    if(i.author === authorName){
      posts++;
      // console.log(i)
    }
  }
  for(let j in listOfPosts){
    for(let m in listOfPosts[j].comments){
      if(listOfPosts[j].comments[m].author === authorName){
        comments++;
      }
    }
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  const queu = people.map(e => +e);
  let sum = 0;
  for(const i of queu){
    if(i === 25){
      sum+=i
    }else{
      sum = sum - (i - 25);
    }
    if(sum < 0){
      return 'NO'
    }
    sum+=25
  }
  if(sum){
    return 'YES';
  }
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
